﻿namespace SampleWebApp.StockReader.Models
{
    /// <summary>
    /// This is the format of the CSV that you can get from https://stooq.com.
    /// </summary>
    public class StockInfo
    {
        public string Symbol { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Open { get; set; }
        public string High { get; set; }
        public string Low { get; set; }
        public string Close { get; set; }
        public string Volume { get; set; }
    }
}
