﻿using CsvHelper;
using SampleWebApp.StockReader.Models;
using System;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SampleWebApp.StockReader
{
    public class StockClient
    {
        public HttpClient HttpClient { get; set; }

        public StockClient()
        {
            this.HttpClient = new HttpClient();
        }

        public async Task<string> ReadFromStockUrlAsync(string stockCode)
        {
            var url = $"https://"+ $"stooq.com/q/l/?s={stockCode}&f=sd2t2ohlcv&h&e=csv";

            using (var msg = new HttpRequestMessage(HttpMethod.Get, new Uri(url)))
            {
                msg.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/csv"));
                using var resp = await this.HttpClient.SendAsync(msg);
                resp.EnsureSuccessStatusCode();

                using var s = await resp.Content.ReadAsStreamAsync();
                using var sr = new StreamReader(s);
                using var futureoptionsreader = new CsvReader(sr, CultureInfo.CurrentCulture);

                var enumerable = futureoptionsreader.GetRecords<StockInfo>();

                if (enumerable != null)
                {
                    var stockValue = enumerable.ToList()[0].Close;

                    return $"{stockCode} quote is ${stockValue} per share";
                }
            }

            return "No Stock Info";
        }

        /// <summary>
        /// Method used by the ChatBot to send messages.
        /// </summary>
        /// <param name="chatRoomId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<string> PostToChatAsync(int chatRoomId, string message)
        {
            var postPort = ConfigurationManager.AppSettings.Get("chatPortForBot");
            var url = $"https://localhost:{postPort}/Home/StockMessage";

            using (var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url)))
            {
                dynamic jsonObj = new ExpandoObject();
                jsonObj.Message = message;
                jsonObj.ChatRoomId = chatRoomId;

                var json = JsonSerializer.Serialize<ExpandoObject>(jsonObj);

                var todoItemJson = new StringContent(
                    json,
                    Encoding.UTF8,
                    "application/json");

                request.Content = todoItemJson;

                HttpResponseMessage response = await HttpClient.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    //TODO: Log in future
                }
            }

            return "No Stock Info";
        }
    }
}
