﻿"use strict";

var connection = new signalR.HubConnectionBuilder()
    .configureLogging(signalR.LogLevel.Debug)
    .withUrl('/chatHub', {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
    .build();

//Disable the send button until connection is established.
document.getElementById("submitButton").disabled = true;

connection.on('ReceiveMessage', addMessageToChat);
connection.on('SendBotMessage', addMessageToChat);

connection.start().then(function () {

        document.getElementById("submitButton").disabled = false;

    }).catch(error => {
        console.error(error.message);
    });

function sendMessageToHub(chatRoomId, user, message) {
    connection.invoke('SendMessage', chatRoomId, user, message);
}