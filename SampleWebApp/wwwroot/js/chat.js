﻿class Message {
    constructor(chatRoomId, username, text, when) {
        this.chatRoomId = chatRoomId;
        this.userName = username;
        this.text = text;
        this.when = when;
    }
}

// userName is declared in razor page.
const usernameConst = userName;

const textInput = document.getElementById('messageText');
const chatRoomSelect = document.getElementById('chatRoomSelect');
const chatRoomIdHidden = document.getElementById('chatRoomIdHidden');
const when = document.getElementById('when');
const chat = document.getElementById('chat');
const messagesQueue = [];


$("#chatRoomSelect").on("change", function () {
    var selectedId = $(this).val();

    window.location.href = '/Home/Index/' + selectedId;
});

document.getElementById('submitButton').addEventListener('click', () => {
    let currentdate = new Date();

    if (when != null) {

        when.innerHTML =
            (currentdate.getMonth() + 1) + "/"
            + currentdate.getDate() + "/"
            + currentdate.getFullYear() + " "
            + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    }
});

function clearInputField() {
    messagesQueue.push(textInput.value);
    textInput.value = "";
}

function sendMessage() {
    
    let text = messagesQueue.shift() || "";
    if (text.trim() === "") return;
    
    let when = new Date();
    let chatRoomId = parseInt(chatRoomSelect.value);
    let message = new Message(chatRoomId, usernameConst, text, when);

    sendMessageToHub(chatRoomId, message.userName, message.text);
}

function addMessageToChat(chatRoomId, user, message) {

    // Avoid receiving messages targeted to other Chat Room
    if (chatRoomId != parseInt(chatRoomSelect.value)) { return; }

    let isCurrentUserMessage = user === usernameConst;

    let mdContainer = document.createElement('div');
    mdContainer.className = isCurrentUserMessage ? "col-md-6 offset-md-6" : "col-md-6";

    let container = document.createElement('div');
    container.className = isCurrentUserMessage ? "container darker" : "container";

    let sender = document.createElement('p');
    sender.className = "sender";
    sender.innerHTML = user;
    let text = document.createElement('p');
    text.innerHTML = message;

    let when = document.createElement('span');
    when.className = isCurrentUserMessage ? "time-left" : "time-right";
    let currentdate = new Date();
    when.innerHTML = 
        (currentdate.getMonth() + 1) + "/"
        + currentdate.getDate() + "/"
        + currentdate.getFullYear() + " "
        + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

    container.appendChild(sender);
    container.appendChild(text);
    container.appendChild(when);

    mdContainer.appendChild(container);

    // Count the divs
    var length = $('#chat').find('>div').length;

    if (length > 50)
    {
        // Remove one element
        $('#chat').find('div').first().remove();
    }
    // Add one element
    chat.appendChild(mdContainer);
}
