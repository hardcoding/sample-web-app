﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace SampleWebApp.Models
{
    /// <summary>
    /// It extends the Identity User, to include a Messages collection.
    /// One AppUser has many Messages. This will be configured at ApplicationDbContext.cs.
    /// </summary>
    public class AppUser: IdentityUser
    {
        public AppUser()
        {
            Messages = new HashSet<Message>();
        }

        public virtual ICollection<Message> Messages { get; set; }
    }
}