﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SampleWebApp.Models
{
    public class Message
    {
        public int Id { get; set; }

        public int ChatRoomId { get; set; }

        /// <summary>
        /// Username generating the message.
        /// </summary>
        public string UserName { get; set; }

        [Required]
        /// <summary>
        /// Text sent in the chat room.
        /// </summary>
        public string Text { get; set; }

        [Required]
        /// <summary>
        /// DateTime for the message.
        /// </summary>
        public DateTime When { get; set; }

        /// <summary>
        /// User logged in, according Identity.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Application user that sends the message.
        /// </summary>
        public virtual AppUser Sender { get; set; }

        /// <summary>
        /// Chat Room
        /// </summary>
        public virtual ChatRoom ChatRoom { get; set; }

        public Message()
        { 
            When = DateTime.Now;
        }
    }
}