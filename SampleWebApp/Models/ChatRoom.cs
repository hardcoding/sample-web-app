﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SampleWebApp.Models
{
    [Table("ChatRooms")]
    public class ChatRoom
    {
        public int Id { get; set; }

        [Required]
        /// <summary>
        /// Username generating the message.
        /// </summary>
        public string Name { get; set; }

        public ChatRoom(string name)
        {
            this.Name = name;
        }
    }
}
