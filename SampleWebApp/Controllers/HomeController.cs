﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SampleWebApp.Data;
using SampleWebApp.Hubs;
using SampleWebApp.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SampleWebApp.Controllers
{
    [Microsoft.AspNetCore.Authorization.Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public readonly ApplicationDbContext _context;
        public readonly UserManager<AppUser> _userManager;
        private readonly IHubContext<ChatHub> _hubContext;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, UserManager<AppUser> userManager, IHubContext<ChatHub> hubcontext)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
            _hubContext = hubcontext;
        }

        public async Task<IActionResult> Index(int chatRoomId)
        {
            // Get current logged user
            var currentUser = await _userManager.GetUserAsync(User);

            if (User?.Identity?.IsAuthenticated == true)
            {
                ViewBag.CurrentUserName = currentUser.UserName;
            }
            var messages = await _context.Messages.ToListAsync();

            // Get available Chat Rooms
            List<SelectListItem> availableChatRooms = new();

            var task = Task.Run(async () => await _context.ChatRooms.ToListAsync());
            var list = task.Result;

            // Create the data source for the ChatRoom DropDownList
            availableChatRooms.Add(new SelectListItem() { Text = "-- Please select a value --", Value = "" });
            
            foreach (var item in list)
            {
                var selectItem = new SelectListItem() { Text = item.Name, Value = item.Id.ToString() };

                if (chatRoomId > 0 && item.Id == chatRoomId)
                {
                    selectItem.Selected = true;
                    ViewBag.CurrentChatRoom = item.Name;
                }

                availableChatRooms.Add(selectItem);
            }

            ViewBag.AvailableChatRooms = availableChatRooms;
            ViewBag.CurrentChatRoomId = chatRoomId;

            // Return the messages for the correspoding ChatRoom
            return View(messages.Where(m => m.ChatRoom.Id.Equals(chatRoomId)));
        }

        public async Task<IActionResult> Create(Message message)
        {
            if (ModelState.IsValid)
            {
                // The stock command won't be saved on the database as a post.
                if (message.Text != null && !message.Text.StartsWith("/stock="))
                { 
                    // Add the message to the database
                    string username = User?.Identity?.Name;
                    message.UserName = username ?? string.Empty;
                    var sender = await _userManager.GetUserAsync(User);
                    message.UserId = sender.Id;
                    await _context.Messages.AddAsync(message);
                    await _context.SaveChangesAsync();                
                }

                return Ok();
            }
            return Error();
        }

        /// <summary>
        /// This method will send a message to the ChatRoom, to answer the "/stock=stock_code" command.
        /// This message will be sent by the Bot.
        /// </summary>
        /// <param name="jsonData">Json Data including the message that is an answer to the "/stock=stock_code" command.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Home/StockMessage")]
        public async Task<ActionResult> SendStockMessage([FromBody] dynamic jsonData)
        {
            dynamic data = JsonConvert.DeserializeObject<dynamic>(jsonData.ToString());

            await _hubContext.Clients.All.SendAsync("SendBotMessage", (int)data.ChatRoomId, "bot@system", (string)data.Message);

            return Ok();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
