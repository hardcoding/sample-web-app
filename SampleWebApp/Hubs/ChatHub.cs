﻿using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using System.Text;
using System.Threading.Tasks;

namespace SampleWebApp.Hubs
{
    public class ChatHub: Hub
    {
        /// <summary>
        /// This method is exposed by the backend to send a message to the other users connected to the
        /// chat room. If the message is a "/stock=" command, then a message will be sent to the decoupled
        /// StockBot program.
        /// </summary>
        /// <param name="chatRoomId">Chat Room Id.</param>
        /// <param name="user">User sending the message.</param>
        /// <param name="message">Text message.</param>
        /// <returns></returns>
        public async Task SendMessage(int chatRoomId, string user, string message)
        {
            if (message.StartsWith("/stock="))
            {
                var stockCode = message[7..].Trim();

                SendMessageToBot(chatRoomId, stockCode);
            }

            await Clients.All.SendAsync("ReceiveMessage", chatRoomId, user, message);
        }

        /// <summary>
        /// This method is exposed by the backend to communicate with the RabbitMQ "stock_queue" queue.
        /// Basically, it will send the chatRoomId and the stockCode to the queue.
        /// </summary>
        /// <param name="chatRoomId">Chat Room Id.</param>
        /// <param name="stockCode">Stock Code.</param>
        public void SendMessageToBot(int chatRoomId, string stockCode)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: "stock_queue",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var body = Encoding.UTF8.GetBytes($"{chatRoomId}::{stockCode}");

            channel.BasicPublish(exchange: "",
                                 routingKey: "stock_queue",
                                 basicProperties: null,
                                 body: body);
        }
    }
}
