﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SampleWebApp.StockReader;
using System;
using System.Text;

namespace StockBot
{

    /// <summary>
    /// Stock Bot demo.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            // Declare RabbitMQ Stock Queue
            channel.QueueDeclare(queue: "stock_queue",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                try
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($"Message received: {message}");

                    var messageParts = message.Split("::");


                    // Read stock value from Url
                    StockClient client = new();
                    int chatRoomId = int.Parse(messageParts[0]);
                    string stockMessage = await client.ReadFromStockUrlAsync(messageParts[1]);

                    Console.WriteLine($"Sending a message to the Chat Room: {stockMessage}");
                    Console.WriteLine($"....");

                    // Send POST message to stock - message endpoint at HomeController
                    await client.PostToChatAsync(chatRoomId, stockMessage);

                    // Dispose client
                    client.HttpClient.Dispose();
                    client = null;
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null) { ex = ex.InnerException; }

                    Console.WriteLine($"Managed error: {ex.Message}");
                }
            };
            channel.BasicConsume(queue: "stock_queue",
                                 autoAck: true,
                                 consumer: consumer);

            // This Console App will stop if you press ENTER.
            Console.ReadLine();
        }
    }
}
