# README #

This project was created using ASP.NET Core Web App (Model-View-Controller) with Indivvidual User Accounts (Authentication Type) and .NET 5.0 using Visual Studio 2019.
I did not used Visual Studio 2022 because there is a problem with SignalR that is not solved by Visual Studio 2022.

I used a LocalDB Database that can be accessed using the Sql Object Explorer. (View > SQL Server Object Explorer).

I used the RabbitMQ Server 3.9.14.exe with Erlang/OTP 24.3.3.

[Downloads - Erlang/OTP](https://www.erlang.org/downloads)

[Downloads - RabbitMQ](https://www.rabbitmq.com/install-windows.html#installer)

(*) When I run the SampleWebApp MVC app, I needed to install the System.Diagnostics.PerformanceCounter in order to make the SendStockMessage() method at HomeController, because SignalR requested me to refer this library.

### How do I get set up? ###

* First you must run the RabbitMQ Server
* Open the Package Manager Console at Visual Studio and run Update-Database command.
* Then you should start SampleWebApp MVC application
* Then you must start StockBot console application with the executable file generated at this project.
* At the https://localhost:44335/ URL you should register two different users.
* Send messages from two different users at two different browsers.

You can start SampleWebApp and StockBot using <Multiple Startup Projects> at Visual Studio 2019.

Remark: I did the development using Google Chrome as Navigator.

### Improvements ###

This project is an adaptation from https://www.youtube.com/watch?v=RUZLIh4Vo20 as a POC.

Stock client is in separated project, this can be done in main. It is sending stock response thru an unprotected endpoint in main app.

Code is not abstracted in a way to support new commands.

The endpoint / StockMessage is unprotected, this is not necessary, you could just send to the hub the message in the Rabbit consumer.

Stock URL should be moved to config file. It is recommended to move the queue name and host to config.

It is selecting all messages, doing a where by chatroom 

var messages = await _context.Messages.ToListAsync();
View(messages.Where(m=> m.ChatRoom.Id.Equals(chatRoomId))); 

This is not efficient. It is recommended to write more tests.
