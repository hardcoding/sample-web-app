using NUnit.Framework;
using SampleWebApp.StockReader;
using System.Threading.Tasks;

namespace SampleWebApp.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task StockReaderTest()
        {
            StockClient stockClient = new();
            var text = await stockClient.ReadFromStockUrlAsync("aapl.us");

            if (text.Contains("quote is"))
            {
                Assert.Pass();
            }
            
            Assert.Fail();
        }
    }
}